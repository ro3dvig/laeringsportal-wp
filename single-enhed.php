<?php 


	$bgToggle = get_field('bg_toggle');
	$topImage = get_field('baggrundsbillede');
	$topColor = get_field('baggrundsfarve');

	if( $bgToggle != 'color' ) :
		$topBg = 'background-image: url(' . $topImage['url'] . ');';
	else: 
		$topBg = 'background-color: ' . $topColor . ';';
	endif;


	$logo = get_field('logo');
	$manchet = get_field('beskrivelse');
	$content = get_field('indhold');
?>



<?php get_header(); ?>


<?php //if (have_posts()): while (have_posts()) : the_post();?>

	  <header>
	    <div id="page-header" style="<?php echo $topBg; ?>">
	      <img class="logo" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>"/>
	    </div>
	  </header>



	  <a id="main-content"></a>
	  
	  <main id="page">
	  	<nav aria-label="Brødkrummesti" class="breadcrumb">
			<?php if(function_exists('bcn_display')) { bcn_display(); }?>
		</nav>
	  	
	  	<h1><?php the_title(); ?></h1> 

	  	<section>	      
	      <p class="manchet"><?php echo $manchet; ?></p>

	      <article>

	      	<?php
	      		require_once( get_template_directory() . '/template-parts/flex-content.php' );
	    	?>

	  	  </article>
	    </section>

	    <aside>
	    	<?php require_once( get_template_directory() . '/template-parts/sidebar-infobox.php' ); ?>

	   		<?php require_once( get_template_directory() . '/template-parts/widget.php' ); ?>
	    </aside>

	  </main>


<?php get_footer(); ?>
