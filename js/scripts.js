
(function ($, root, undefined) {
	
	$(function () {
		
		'use strict';

		// DOM ready, take it away
	

//--------------------Start anonymous function-----------------		


//Vars
var modalActive = false;



//Init flickity
function initFlickity(){

  $('.grid-carousel').flickity({
    // options
    groupCells: "70%",
    wrapAround: true,
    pageDots: false
  });


  //Enable modal functionality
  var gridElements = $( ".grid-carousel .carousel-grid-cell" );
  
  for(var i=0; i<gridElements.length; i++ ){

    var item = $(gridElements[i]);
    var open = item.find('.open');

    item.data( 'itemId', i );

    open.click(function(){

      openModal(this);

    });
  }
}


//Replace Jquery 'closest' for IE compitibility
function myClosest(el, selector) {
    var matchesFn;
    // find vendor prefix
    ['matches','webkitMatchesSelector','mozMatchesSelector','msMatchesSelector','oMatchesSelector'].some(function(fn) {
        if (typeof document.body[fn] == 'function') {
            matchesFn = fn;
            return true;
        }
        return false;
    })
    var parent;
    // traverse parents
    while (el) {
        parent = el.parentElement;
        if (parent && parent[matchesFn](selector)) {
            return parent;
        }
        el = parent;
    }
    return null;
}








//-------------Grid functions-------------
function openModal(clickId){

  var section = myClosest(clickId, 'section'); //clickId.closest('section');
  var itemId = $(clickId).parent().data('itemId');
  var item = $(clickId).prev();
  
  var modal = $('#modal');
  var active = $('.modal');
  
  var content = item.clone();

  //console.log(itemId);



  if(!active.length){
    modal.clone().removeAttr('id').addClass('modal').append(content).appendTo( section ).slideDown(300);
    

    $('.modal .close').click(function(){
      closeModal(this);
    });

    setTimeout(initModalFlikity, 1000);


    modalActive =  itemId;


  }else if(modalActive != itemId){

    
    active.fadeOut(200, function(){
      active.find('.item').remove();
      active.append(content).appendTo( section );


      $('.modal .close').click(function(){
        closeModal(this);
      });




      setTimeout(initModalFlikity, 1000);

      active.fadeIn(200);

    });    


    modalActive =  itemId;


  }else{
    return;
  }


  /*
  var section = clickId.closest('section');
  var item = clickId.prev();
  var modal = $('#modal');
  var active = $('.modal');
  
  var content = item.clone();

   console.log(item);

  if(!active.length){
    modal.clone().removeAttr('id').addClass('modal').append(content).appendTo( section ).slideDown(300);  
  }else{
    return;
  }*/
}


function closeModal(id){
  var modal = myClosest(id, '.modal'); //id.closest('.modal')

  $(modal).slideUp(200, function(){
    $(modal).remove();
    $('.modal-carousel').flickity('destroy');

    modalActive = false;
  });
}


function initModalFlikity(){

	console.log('flickity runnin');

  $('.modal-carousel').flickity('destroy');

  $('.modal-carousel').flickity({
    // options
    contain: true,
    pageDots: false,
    autoPlay: true,
    wrapAround: true
  });

}


/*
//Nav
// When the user scrolls the page, execute myFunction
window.onscroll = function() {myFunction()};

// Get the navbar
var navbar = document.getElementById("navbar");

// Get the offset position of the navbar
var sticky = navbar.offsetTop;

// Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}
*/

/*
$(document).ready(function(){
   
});
*/

//init functions
 initFlickity();


// =================================
// Accordion
// =================================
$('.accordion_toggleAll').on('click touchend',function(e){
    $(this).toggleClass('open');
    if($(this).hasClass('open')){
      $(this).next('.accordion').find('input[type=checkbox]').each(function () {
          $(this).prop('checked', false);
      });
    }
    if(!$(this).hasClass('open')){
      $(this).next('.accordion').find('input[type=checkbox]').each(function () {
          $(this).prop('checked', true);
      });
    }

});


// =================================
// Tidslinjer
// =================================
var buttons = document.querySelectorAll('.tlContainer .btn'), i;
var activeFS = null;

if(buttons.length > 0){
      console.log(buttons);

      for (i = 0; i < buttons.length; ++i) {
        buttons[i].addEventListener('click', function(){
            fullscreen(myClosest(this, '.tlContainer'));
            activeFS = myClosest(this, '.tlContainer');
       });
      }

      // when you are in fullscreen, ESC and F11 may not be trigger by keydown listener. 
      // so don't use it to detect exit fullscreen
      document.addEventListener('keydown', function (e) {
        console.log('key press' + e.keyCode);
      });

      // detect enter or exit fullscreen mode
      document.addEventListener('webkitfullscreenchange', fullscreenChange);
      document.addEventListener('mozfullscreenchange', fullscreenChange);
      document.addEventListener('fullscreenchange', fullscreenChange);
      document.addEventListener('MSFullscreenChange', fullscreenChange);
}

function fullscreen(iframeID) {
    // check if fullscreen mode is available
    if (document.fullscreenEnabled || 
      document.webkitFullscreenEnabled || 
      document.mozFullScreenEnabled ||
      document.msFullscreenEnabled) {
      
      // which element will be fullscreen
      var iframe = iframeID;//document.querySelector('.tlContainer iframe');
      // Do fullscreen
      if (iframe.requestFullscreen) {
        iframe.requestFullscreen();
      } else if (iframe.webkitRequestFullscreen) {
        iframe.webkitRequestFullscreen();
      } else if (iframe.mozRequestFullScreen) {
        iframe.mozRequestFullScreen();
      } else if (iframe.msRequestFullscreen) {
        iframe.msRequestFullscreen();
      }
    }
    else {
      document.querySelector('.error').innerHTML = 'Your browser is not supported';
    }
}


function fullscreenChange() {
  if (document.fullscreenEnabled ||
       document.webkitIsFullScreen || 
       document.mozFullScreen ||
       document.msFullscreenElement) {
    console.log('enter fullscreen');
  }
  else{
    console.log('exit fullscreen');
  }

  // force to reload iframe once to prevent the iframe source didn't care about trying to resize the window
  // comment this line and you will see
  var iframe = activeFS;//document.querySelector('.tlContainer iframe');
  iframe.src = iframe.src;
}



  // =================================
  // Archive-series sorting
  // =================================
  var $sortingSelector = $('.archive_series .sort');
  function sortArchiveseries(sorting){
    $('.archive_series ul li').sort(sort_li).appendTo('.archive_series ul');
     function sort_li(a, b) {
       return ($(b).data(sorting)) < ($(a).data(sorting)) ? 1 : -1;
     }
  };
  $sortingSelector.on('change',function(e){
    var sorting = $(this).val();
    sortArchiveseries(sorting);
  });



//--------------------End anonymous function-----------------		
	});
	
})(jQuery, this);






