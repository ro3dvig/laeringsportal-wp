

jQuery( document ).ready(function() {
    jQuery('.ui.dropdown').dropdown({
    message: {
        addResult     : 'Tilføj <b>{term}</b>',
        count         : '{count} valgt',
        maxSelections : 'Max {maxCount} valg',
        noResults     : 'Ingen resultater fundet.',
        onChange      : function(value, text, $choice) { console.log('changed'); }
    }
  });

});






var kildekatalog = function() {
   var app = angular.module('kildekatalog', ['ngSanitize','ui.bootstrap']);

    app.directive('kilder', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var kilderID = attrs.kilder;

                scope.items = window[kilderID];
                scope.original = window[kilderID];
            }
        }
    });

    app.directive('listen', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                element[0].addEventListener('change', function() {
                    scope[attrs.listen] = this.value.split(',');

                    if (! scope[attrs.listen][0]) {
                        scope[attrs.listen] = [];
                    }

                    setTimeout(function() {
                        jQuery(element[0]).closest('.ui.dropdown').dropdown('hide');
                    }, 200);

                    scope.$apply();
                });
            }
        }
    });

    app.controller('kildeController', ['$scope', function($scope) {
        $scope.items = [];

        $scope.original = [];

        // Sorting parameters
        $scope.period = 'all';
        $scope.meta = '';
        $scope.themes = [];
        $scope.subthemes = [];
        $scope.types = [];

        $scope.sort = {
            direction: 'asc',
            attr: null
        };

        // Watchers
        ['period', 'meta', 'themes', 'subthemes', 'types', 'sort.attr', 'sort.direction'].forEach(function(item) {
            $scope.$watch(item, function(value, old) {
                if (value == old) return;

                if (item == 'period') {
                    setTimeout(function() {
                        jQuery('#period').closest('.ui.dropdown').dropdown('hide');
                        console.log('hide period!');
                    }, 1000);
                }

                $scope.doSort();
            });
        });

        $scope.setSorter = function(value) {
            if ($scope.sort.attr == value) {
                switch ($scope.sort.direction) {
                    case 'asc':
                        $scope.sort.direction = 'desc';
                        break;
                    case 'desc':
                        $scope.sort.direction = 'asc';
                        $scope.sort.attr = null;
                        break;
                }
            } else {
                $scope.sort.direction = 'asc';
                $scope.sort.attr = value;
            }
        };

        $scope.doSort = function() {
            var items = angular.copy($scope.original);

            items = $scope.sortPeriod(items);
            items = $scope.sortMeta(items);
            items = $scope.sortThemes(items);
            items = $scope.sortSubthemes(items);
            items = $scope.sortTypes(items);

            $scope.items = $scope.arrange(items);
        };

        $scope.arrange = function(items)
        {
            if (! $scope.sort.attr)
                return items;

            switch ($scope.sort.attr) {
                case 'title':

                    items = items.sort(function(a, b) {
                        var reverse = $scope.sort.direction == 'desc' ? -1 : 1;

                        return reverse * ((a.title > b.title) - (b.title > a.title));
                    });
                    break;
                case 'meta':
                    items = items.sort(function(a, b) {
                        var reverse = $scope.sort.direction == 'desc' ? -1 : 1;

                        return reverse * ((a.meta[0] > b.meta[0]) - (b.meta[0] > a.meta[0]));
                    });
                    break;
                case 'year':
                    items = items.sort(function(a, b) {
                        var reverse = $scope.sort.direction == 'desc' ? -1 : 1;


                        return reverse * (b.time.start - a.time.start);
                    });
                    break;
            }

            return items;
        };

        $scope.sortTypes = function(items) {
            if (! $scope.types.length) {
                return items;
            }

            items = items.filter(function(item) {
                var has_type = false;

                $scope.types.forEach(function(type) {
                    if (item.types.indexOf(type) > -1) {
                        has_type = true;
                        return has_type;
                    }
                });

                return has_type;
            });

            return items;
        };

        $scope.sortThemes = function(items) {
            if (! $scope.themes.length) {
                return items;
            }

            items = items.filter(function(item) {
                var has_theme = false;

                $scope.themes.forEach(function(theme) {
                    if (item.themes.indexOf(theme) > -1) {
                        has_theme = true;
                        return has_theme;
                    }
                });

                return has_theme;
            });

            return items;
        };

        // subtheme
        $scope.sortSubthemes = function(items) {
            if (! $scope.subthemes.length) {
                return items;
            }

            items = items.filter(function(item) {
                var has_subtheme = false;

                $scope.subthemes.forEach(function(theme) {
                    if (item.subthemes.indexOf(theme) > -1) {
                        has_subtheme = true;
                        return has_subtheme;
                    }
                });

                return has_subtheme;
            });

            return items;
        };

        $scope.sortMeta = function(items) {
            if (! $scope.meta) {
                return items;
            }

            items = items.filter(function(item) {
                return item.meta.indexOf($scope.meta) > -1;
            });

            return items;
        }

        $scope.sortPeriod = function(items) {

            if ($scope.period == 'all') {
                return items;
            }

            items = items.filter(function(item) {
                var from = parseInt($scope.period, 10);
                var to = from + 99;

                return (item.time.start >= from && item.time.start <= to) || (item.time.end >= from && item.time.end <= to);
            });

            return items;
        };

        $scope.arrayToString = function(string){
            return string.join(", ");
        };
    }]);

    return app;
};

kildekatalog();



