<?php
/**
 * The template for displaying archive series.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Sa2016
	 */

get_header(); 

?>
	  <header>
	    <div id="page-header" style="background-image: url(https://www.kilderne.dk/wp-content/uploads/2019/05/Rigsarkivet_Heropicture_Kildepakker.jpg);">
	      <img class="logo" src="https://kilderne.dk/wp-content/uploads/2020/10/Arkivserier_logo.svg" alt="Kildepakkerne Logo"/>
	    </div>
	  </header>

	  <a id="main-content"></a>

	  <main id="page" class="tilbud">
	  	<nav aria-label="Brødkrummesti" class="breadcrumb">
			<?php if(function_exists('bcn_display')) { bcn_display(); }?>
		</nav>



	  	<h1><?php the_title(); ?></h1> 

	  	<section>	      	      
	      <p class="manchet"><?php if(get_field('manchet')): ?><?php the_field('manchet'); ?><?php endif; ?></p>
	      <article>

	      	<?php the_content(); ?> 

	      	<?php // Render additional content
				if( have_rows('additional_content') ): while ( have_rows('additional_content') ) : the_row();
					
					if( get_sub_field('accordion') ): //If accordion
					?>

					<span class="accordion_toggleAll">
						<span>+ Åben alle</span><span>- Luk alle</span>
					</span>
					<ul class="accordion additional_content_block">
					<?php if( have_rows('accordion') ): while ( have_rows('accordion') ) : the_row(); ?>
						<li>
							<input type="checkbox" checked>
							<h5><?php the_sub_field('title');?></h5>
							<span class="state"></span>
							<div class="accordion-content"><?php the_sub_field('text');?></div>
						</li>
					<?php endwhile; else: endif; ?>
					</ul>
					<?php
				   	//End of If accordion  -->
					endif;



				endwhile; else : endif;
			?>

	      	<?php // Render archives
				if( get_field('records') ):
					include('template-parts/archive_single.php');
				endif;
			?>	

	      	<?php
	      		//require_once( get_template_directory() . '/template-parts/flex-content.php' );
	    	?>

	  	  </article>
	    </section>

	    <aside>
		    <?php require_once( get_template_directory() . '/template-parts/widget.php' ); ?>
	    </aside>

	    <?php require_once( get_template_directory() . '/template-parts/child-list.php' ); ?>

	  </main>


<?php get_footer(); ?>


