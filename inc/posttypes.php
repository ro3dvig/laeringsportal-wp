<?php

// Læringstilbud
function tilbud_post_type() {

	$labels = array(
		'name'                  => _x( 'Læringstilbud', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Læringstilbud', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Læringstilbud', 'text_domain' ),
		'name_admin_bar'        => __( 'Læringstilbud', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'Alle læringstilbud', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Tilføj nyt læringstilbud', 'text_domain' ),
		'new_item'              => __( 'Nyt læringstilbud', 'text_domain' ),
		'edit_item'             => __( 'Rediger læringstilbud', 'text_domain' ),
		'update_item'           => __( 'Opdater læringstilbud', 'text_domain' ),
		'view_item'             => __( 'Se læringstilbud', 'text_domain' ),
		'view_items'            => __( 'Se læringstilbud', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Læringstilbud', 'text_domain' ),
		'description'           => __( 'Enkelt læringstilbud', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'revisions', 'page-attributes'),
		//'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 2,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'rewrite' => array('slug' => 'laeringstilbud', 'with_front' => false),
	);
	register_post_type( 'tilbud', $args );

}
add_action( 'init', 'tilbud_post_type', 0 );


// Register Custom Post Type
function widget_post_type() {

	$labels = array(
		'name'                  => _x( 'Widget', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Widget', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Widget', 'text_domain' ),
		'name_admin_bar'        => __( 'Widget', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'Alle widgets', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Tilføj ny widget', 'text_domain' ),
		'new_item'              => __( 'Ny widget', 'text_domain' ),
		'edit_item'             => __( 'Rediger widget', 'text_domain' ),
		'update_item'           => __( 'Opdater widget', 'text_domain' ),
		'view_item'             => __( 'Se widget', 'text_domain' ),
		'view_items'            => __( 'Se widget', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Widgets', 'text_domain' ),
		'description'           => __( 'Enkel widget', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes', 'post-formats' ),
		//'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 4,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => false,
		'capability_type'       => 'page',
	);
	register_post_type( 'widgets', $args );

}
add_action( 'init', 'widget_post_type', 0 );



// Register Custom Post Type
function kildekatalog_post_type() {

	$labels = array(
		"name" 					=> _x( 'Kildekataloger', 'Post Type General Name', 'text_domain' ),
        "singular_name" 		=> _x( 'Kildekatalog', 'Post Type Singular Name', 'text_domain' ),
        "menu_name" 			=> __( 'Kildekataloger', 'text_domain' ),
        "add_new" 				=> __( 'Tilføj ny', 'text_domain' ),
        "add_new_item" 			=> __( 'Tilføj ny Kildekatalog', 'text_domain' ),
        "new_item" 				=> __( 'Ny Kildekatalog', 'text_domain' ),
        "edit_item" 			=> __( 'Redigér Kildekatalog', 'text_domain' ),
        "view_item" 			=> __( 'Vis Kildekatalog', 'text_domain' ),
        "all_items" 			=> __( 'Alle Kildekataloger', 'text_domain' ),
        "search_items" 			=> __( 'Søg i Kildekataloger', 'text_domain' ),
        "not_found" 			=> __( 'Ingen Kildekataloger fundet', 'text_domain' ),
        "not_found_in_trash" 	=> __( 'Ingen Kildekataloger fundet i papirkurv', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Kildekatalog', 'text_domain' ),
		'labels'                => $labels,
		'public'				=> false,
	    'show_ui' 				=> true,
	    'has_archive'			=> false,
	    'supports' 				=> ['title']
	);

	register_post_type( 'kildekatalog', $args );

}
add_action( 'init', 'kildekatalog_post_type', 0 );



// Register Custom Post Type
function arkivserier_post_type() {

	$labels = array(
		"name" 					=> _x( 'Arkivserier', 'Post Type General Name', 'text_domain' ),
        "singular_name" 		=> _x( 'Arkivserier', 'Post Type Singular Name', 'text_domain' ),
        "menu_name" 			=> __( 'Arkivserier', 'text_domain' ),
        "add_new" 				=> __( 'Tilføj ny', 'text_domain' ),
        "add_new_item" 			=> __( 'Tilføj ny Arkivserie', 'text_domain' ),
        "new_item" 				=> __( 'Ny Arkivserie', 'text_domain' ),
        "edit_item" 			=> __( 'Redigér Arkivserie', 'text_domain' ),
        "view_item" 			=> __( 'Vis Arkivserie', 'text_domain' ),
        "all_items" 			=> __( 'Alle Arkivserier', 'text_domain' ),
        "search_items" 			=> __( 'Søg i Arkivserier', 'text_domain' ),
        "not_found" 			=> __( 'Ingen Arkivserier fundet', 'text_domain' ),
        "not_found_in_trash" 	=> __( 'Ingen Arkivserier fundet i papirkurv', 'text_domain' ),
	);

	
	$args = array(
		'label'                 => __( 'Arkivserier', 'text_domain' ),
		'description'           => __( 'Enkelt arkivserie', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'revisions', 'page-attributes'),
		//'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 4,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'rewrite' => array('slug' => 'arkivserier', 'with_front' => false),
	);


	register_post_type( 'archive_series', $args );

}
add_action( 'init', 'arkivserier_post_type', 0 );



// Taxonomy: tema
$labels = array(
    'name'                       => _x( 'Temaer', 'taxonomy general name' ),
    'singular_name'              => _x( 'tema', 'taxonomy singular name' ),
    'search_items'               => __( 'Søg Temaer' ),
    'popular_items'              => __( 'Populære Temaer' ),
    'all_items'                  => __( 'Alle Temaer' ),
    'parent_item'                => null,
    'parent_item_colon'          => null,
    'edit_item'                  => __( 'Rediger tema' ),
    'update_item'                => __( 'Opdater tema' ),
    'add_new_item'               => __( 'Tilføj nyt tema' ),
    'new_item_name'              => __( 'Nyt tema navn' ),
    'separate_items_with_commas' => __( 'Separer Temaer med kommaer' ),
    'add_or_remove_items'        => __( 'Tilføj eller fjern Temaer' ),
    'choose_from_most_used'      => __( 'Vælg fra mest brugte Temaer' ),
    'not_found'                  => __( 'Ingen Temaer fundet.' ),
    'menu_name'                  => __( 'Temaer' ),
);

$args = array(
    'hierarchical'          => false,
    'labels'                => $labels,
    'rewrite'               => array( 'slug' => 'tema' ),
);

register_taxonomy('tema', 'kildekatalog', $args);

// Taxonomy: Undertema
$labels = array(
    'name'                       => _x( 'Undertemaer', 'taxonomy general name' ),
    'singular_name'              => _x( 'undertema', 'taxonomy singular name' ),
    'search_items'               => __( 'Søg Undertemaer' ),
    'popular_items'              => __( 'Populære Undertemaer' ),
    'all_items'                  => __( 'Alle Undertemaer' ),
    'parent_item'                => null,
    'parent_item_colon'          => null,
    'edit_item'                  => __( 'Rediger undertema' ),
    'update_item'                => __( 'Opdater undertema' ),
    'add_new_item'               => __( 'Tilføj nyt undertema' ),
    'new_item_name'              => __( 'Nyt undertema navn' ),
    'separate_items_with_commas' => __( 'Separer Undertemaer med kommaer' ),
    'add_or_remove_items'        => __( 'Tilføj eller fjern Undertemaer' ),
    'choose_from_most_used'      => __( 'Vælg fra mest brugte Undertemaer' ),
    'not_found'                  => __( 'Ingen Undertemaer fundet.' ),
    'menu_name'                  => __( 'Undertemaer' ),
);

$args = array(
    'hierarchical'          => false,
    'labels'                => $labels,
    'rewrite'               => array( 'slug' => 'undertema' ),
);
register_taxonomy('undertema', 'kildekatalog', $args);

// Taxonomy: Typer
$labels = array(
    'name'                       => _x( 'Typer', 'taxonomy general name' ),
    'singular_name'              => _x( 'type', 'taxonomy singular name' ),
    'search_items'               => __( 'Søg Typer' ),
    'popular_items'              => __( 'Populære Typer' ),
    'all_items'                  => __( 'Alle Typer' ),
    'parent_item'                => null,
    'parent_item_colon'          => null,
    'edit_item'                  => __( 'Rediger type' ),
    'update_item'                => __( 'Opdater type' ),
    'add_new_item'               => __( 'Tilføj nyt type' ),
    'new_item_name'              => __( 'Nyt type navn' ),
    'separate_items_with_commas' => __( 'Separer Typer med kommaer' ),
    'add_or_remove_items'        => __( 'Tilføj eller fjern Typer' ),
    'choose_from_most_used'      => __( 'Vælg fra mest brugte Typer' ),
    'not_found'                  => __( 'Ingen Typer fundet.' ),
    'menu_name'                  => __( 'Typer' ),
);

$args = array(
    'hierarchical'          => true,
    'labels'                => $labels,
    'rewrite'               => array( 'slug' => 'type' ),
);

register_taxonomy('type', 'kildekatalog', $args);



