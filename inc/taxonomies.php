<?php
/*Add categories for pages*/
function cats_for_pages() {  
    // Add tag metabox to page
    //register_taxonomy_for_object_type('post_tag', 'page'); 
    
    // Add category metabox to page
    register_taxonomy_for_object_type('category', 'page');  
}
 // Add to the admin_init hook of your theme functions.php file 
add_action( 'init', 'cats_for_pages' );



/*custom taxonomies*/
function niveau_init() {
    // create a new taxonomy
    register_taxonomy(
        'niveau',
        array(
            'tilbud',
            'enhed'
        ),
        array(
            'label' => __( 'Niveauer' ),
            'rewrite' => array( 'slug' => 'niveau' )
        )
    );
}
add_action( 'init', 'niveau_init' );

function fag_init() {
    // create a new taxonomy
    register_taxonomy(
        'fag',
        array(
            'tilbud',
            'enhed'
        ),
        array(
            'label' => __( 'Fag' ),
            'rewrite' => array( 'slug' => 'fag' )
        )
    );
}
add_action( 'init', 'fag_init' );

function emner_init() {
    // create a new taxonomy
    register_taxonomy(
        'emner',
        array(
            'tilbud',
            'enhed'
        ),
        array(
            'label' => __( 'Emner' ),
            'rewrite' => array( 'slug' => 'emne' )
        )
    );
}
add_action( 'init', 'emner_init' );

?>