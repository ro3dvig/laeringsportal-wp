<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<?php the_field('head_snippet', 'option'); //Google Tag manager?>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">	



		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,600,700" rel="stylesheet">

	</head>
	<body <?php body_class(); ?>>
		<?php the_field('body_snippet', 'option'); //Google Tag manager?>
<!--div id="wrapper"-->
	<a class="skip-main" href="#main-content">Spring til indhold</a>

	<nav id="mainmenu" class="mainmenu" aria-label="Hovedmenu">
	  <button class="menu">
	    <em class="hamburger"></em>
	  </button>
	  <div id="logo" class="brand">
	    <a href="<?php echo home_url(); ?>">
      		<img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" alt="Kilderne.dk - Logo" class="logo-img">
      	</a>
	  </div>

	  <?php mainmenu_nav(); ?>				

	</nav>











