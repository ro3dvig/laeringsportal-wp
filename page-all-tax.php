<?php /* Template Name: Alle af én */ ?>

<?php get_header(); ?>

	<header>
		<br/>
		<br/>
		<br/>
		<br/>

	</header>
	<main id="frontpage">

<?php 
	
$args = array(
    'post_type' => 'tilbud',
    'tax_query' => array(
        array(
            'taxonomy' => 'fag',
            'operator' => 'EXISTS'
        ),
    ),
);

$query = new WP_Query( $args );

if ( $query->have_posts() ) {
    while ( $query->have_posts() ) {
        $query->the_post();
        

        echo '<h1>' . the_title() . '</h1>';


        $manchet = get_field('beskrivelse');

        echo '<p>' . $manchet . '</p>';

    }
} else {
    // No posts found.
}
wp_reset_postdata();


 ?>

	</main>


<?php get_footer(); ?>
