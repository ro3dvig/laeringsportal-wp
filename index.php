<?php get_header(); ?>	
    <a id="main-content"></a>
	
	<main>
		<!-- section -->
		<section>

			<h1 id="main-content"><?php _e( 'Latest Posts', 'html5blank' ); ?></h1>

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
