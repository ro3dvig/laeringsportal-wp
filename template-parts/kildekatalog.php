<link rel="stylesheet" media="all" href="<?php bloginfo('template_directory');?>/css/kildekatalog.css"/>

<!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.1.6/semantic.min.css"-->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.1.6/components/dropdown.min.css">


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.1.6/components/transition.min.css">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.1.6/components/label.min.css">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.1.6/components/icon.min.css">





<link rel="stylesheet" media="all" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css"/>




<script src="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.1.6/semantic.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-sanitize.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-route.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.1/select2.js"></script> -->

<script src="<?php bloginfo('template_directory');?>/js/kildekatalog.js"></script>




		<?php while ( have_posts() ) : the_post(); ?>


						<!-- begynd kildekatalog -->
						<?php
						    $code = '';

						    while(strlen($code) < 10 ) {
						        $token = base64_encode(md5(time()));
						        $characters = preg_replace("/[\d]/", "", $token);

						        $code .= strtolower( substr($characters, 0, 10 - strlen($code)) );
						    }

						    $query = new WP_Query([
						        'post_type' => 'kildekatalog',
						        'post_status' => 'publish',
						        'posts_per_page' => -1
						    ]);

						    $items = [];

						    if( $query->posts ) {

						        foreach($query->posts as $katalog) {
						            $themes = wp_get_post_terms( $katalog->ID, 'tema', $args );
						            $subthemes = wp_get_post_terms( $katalog->ID, 'undertema', $args );
						            $types = wp_get_post_terms( $katalog->ID, 'type', $args );

						            $meta = [];

						            switch(get_field('meta', $katalog->ID)) {
						                case 'both':
						                    $meta = ['Grundskolen', 'Gymnasium & HF'];
						                    break;
						                case 'grundskolen':
						                    $meta = ['Grundskolen'];
						                    break;
						                case 'gymnasium':
						                    $meta = ['Gymnasium & HF'];
						                    break;
						            }

						            $_themes = array_map(function($theme) { return $theme->name; }, $themes);
						            $_subthemes = array_map(function($subtheme) { return $subtheme->name; }, $subthemes);
						            $_types = array_map(function($type) { return $type->name; }, $types);
						            $data = [
						                'title' => $katalog->post_title,
						                'meta' => $meta,
						                'time' => [
						                    'start' => (int) get_field('time_start', $katalog->ID),
						                    'end' => get_field('time_end', $katalog->ID) ? (int) get_field('time_end', $katalog->ID) : null
						                ],
						                'description' => get_field('description', $katalog->ID),
						                'links' => get_field('links', $katalog->ID),
						                'themes' => $_themes,
						                'subthemes' => $_subthemes,
						                'types' => $_types,
						                'content' => get_field('content', $katalog->ID)
						            ];

						            $data['token'] = sha1(json_encode($data));

						            $items[] = $data;
						        }

						    }
						?>
						<script>
						    window.<?=$code;?> = <?=json_encode($items);?>;
						</script>
						<div class="kildekatalog" ng-app="kildekatalog" ng-controller="kildeController" kilder="<?=$code;?>">


							<!-- FILTER BOX -->
						    <div class="kildekatalog--filterbox">
				                <div class="form-group">
				                    <label for="">Tidsinterval / periode</label>
				                    <select name="period" id="period" ng-model="period" class="form-control ui fluid dropdown">
				                        <option value="all">Alle perioder</option>
				                        <?php
				                            $years = [];

				                            foreach($items as $item) {
				                                $year = floor($item['time']['start'] / 100) * 100;
				                                $years[$year] = $year."-tallet";
				                            }

				                            ksort($years);

				                            foreach($years as $year => $label) {
				                                printf('<option name="%s">%s</option>', $year, $label);
				                            }
				                        ?>
				                    </select>
				                </div>
				           
				            
				                <div class="form-group">
				                    <label for="">Vælg temaer</label>
				                    <div class="ui fluid multiple search selection dropdown">
				                        <input type="hidden" name="themes" listen="themes">
				                        <i class="dropdown icon"></i>
				                        <div class="default text">Alle temaer</div>
				                        <div class="menu">
				                            <?php
				                                $themes = [];

				                                foreach($items as $item) {
				                                    foreach( $item['themes'] as $theme ) {
				                                        $themes[$theme] = $theme;
				                                    }
				                                }

				                                uksort($themes, function($a, $b) {
				                                    return (strtolower($a) > strtolower($b)) - (strtolower($b) > strtolower($a));
				                                });

				                                foreach($themes as $theme) {
				                                    printf('<div class="item" data-value="%s">%s</div>', $theme, ucfirst($theme));
				                                }
				                            ?>
				                        </div>
				                    </div>
				                </div>
				               
	            
				                <div class="form-group">
				                    <label for="">Vælg undertemaer</label>
				                    <div class="ui fluid multiple search selection dropdown">
				                        <input type="hidden" name="subthemes" listen="subthemes">
				                        <i class="dropdown icon"></i>
				                        <div class="default text">Alle undertemaer</div>
				                        <div class="menu">
				                            <?php
				                                $subthemes = [];

				                                foreach($items as $item) {
				                                    foreach( $item['subthemes'] as $subtheme ) {
				                                        $subthemes[$subtheme] = $subtheme;
				                                    }
				                                }

				                                uksort($subthemes, function($a, $b) {
				                                    return (strtolower($a) > strtolower($b)) - (strtolower($b) > strtolower($a));
				                                });

				                                foreach($subthemes as $subtheme) {
				                                    printf('<div class="item" data-value="%s">%s</div>', $subtheme, ucfirst($subtheme));
				                                }
				                            ?>
				                        </div>
				                    </div>
				                </div>
				           
				            
				                <div class="form-group">
				                    <label for="">Vælg type</label>
				                    <div class="ui fluid multiple search selection dropdown">
				                        <input type="hidden" name="type" listen="types">
				                        <i class="dropdown icon"></i>
				                        <div class="default text">Alle typer</div>
				                        <div class="menu">
				                            <?php
				                                $types = [];

				                                foreach($items as $item) {
				                                    foreach( $item['types'] as $type ) {
				                                        $types[$type] = $type;
				                                    }
				                                }

				                                uksort($types, function($a, $b) {
				                                    return (strtolower($a) > strtolower($b)) - (strtolower($b) > strtolower($a));
				                                });

				                                foreach($types as $type) {
				                                    printf('<div class="item" data-value="%s">%s</div>', $type, ucfirst($type));
				                                }
				                            ?>
				                        </div>
				                    </div>
				                </div>  
						    </div>

						    <div class="kildekatalog--list">
						        <div class="kildekatalog--list__header hidden-xs">
						            <div class="row">
						                <div class="col-md-5">
						                    <div class="pointer" ng-click="setSorter('title');" ng-class="{'active': sort.attr == 'title', 'asc': sort.attr == 'title' && sort.direction == 'asc', 'desc': sort.attr == 'title' && sort.direction == 'desc'}">
						                        <span>Kildenavn</span>
						                        <i class="dropdown icon"></i>
						                    </div>
						                </div>

						                <div class="col-md-5 hidden-xs">
				                        <span>Temaer</span>
						                </div>

						                <div class="col-md-2 hidden-xs">
						                    <div class="pointer" ng-click="setSorter('year');" ng-class="{'active': sort.attr == 'year', 'asc': sort.attr == 'year' && sort.direction == 'asc', 'desc': sort.attr == 'year' && sort.direction == 'desc'}">
						                        <span>Årstal</span>
						                        <i class="dropdown icon"></i>
						                    </div>
						                </div>
						            </div>
						        </div>

						        <div class="kildekatalog--list__items">
						            <div class="row kildekatalog--list__items--item" ng-class="{'closed': ! item.open }" ng-click="item.open = ! item.open;" ng-repeat="item in items" id="{{item.token}}">
						                <div class="kildekatalog--list__items--item__header">
						                    <div class="col-md-1 col-xs-2">
						                        <span class="icon icon-icons_more-close"></span>
						                    </div>
						                    <div class="col-md-4 col-xs-9 heading">
						                        {{ item.title }}
						                    </div>
						                    <div class="col-md-5 meta hidden-xs">
						                        <span ng-repeat="theme in item.themes">
						                            {{ theme }}{{$last ? '' : ', '}}
						                        </span>
						                    </div>
						                    <div class="col-md-2 year hidden-xs">
						                        {{ item.time.start }} <span ng-if="item.time.end">&nbsp;- {{ item.time.end }}</span>
						                    </div>
						                </div>

						                <div class="kildekatalog--list__items--item__body">
						                    <div class="row">
						                        <div class="col-md-11 col-md-offset-1 clearfix">
						                            <div class="visible-xs mobile-meta">
						                                <div class="meta">
						                                    <span ng-repeat="meta in item.meta">
						                                        {{ meta }}
						                                    </span>
						                                </div>

						                                <div class="year">
						                                    {{ item.time.start }} <span ng-if="item.time.end">&nbsp;- {{ item.time.end }}</span>
						                                </div>
						                            </div>
						                            <p>
						                               {{ item.description }}
						                            </p>

						                            <div class="article-link" ng-repeat="link in item.links">
						                                <a target="_blank" href="{{ link.url }}">
						                                   <span class="icon icon-icons_linkpil"></span>
						                                    <h3>{{ link.title }}</h3>
						                                </a>
						                            </div>

						                            <p class="theme">
						                                <strong>Tema:</strong> <span>{{ item.themes.join(', ') }}.</span>
						                            </p>

						                            <div class="description" ng-bind-html="item.content"></div>
						                        </div>
						                    </div>
						                </div>
						            </div>
						        </div>
						    </div>

						</div>
		<?php endwhile; // End of the loop.		?>
