

	      <div class="box info">
	        <!--h4>Om <?php the_title(); ?></h4-->

	        <table class="info">
	          <tbody><tr>
	            <td>Niveau:</td>	           
	            <td><?php the_terms( $post->ID, 'niveau', '', ', ' ); ?></td>
	          </tr>
	          <tr>
	            <td>Fag:</td>
	            <td><?php the_terms( $post->ID, 'fag', '', ', ' ); ?></td>
	          </tr>
	         
	        <?php 			  
	        	if(has_term( '', 'emner')):
	        	?>

	        	<tr>
	        		<td>Emner:</td>
	        		<td><?php the_terms( $post->ID, 'emner', '', ', ' ); ?></td>
	        	</tr>

	        	<?php
	        	endif;
			?>
	        </tbody></table>

		<?php

		// check if the flexible content field has rows of data
		if( have_rows('primary_knap') ):

		     // loop through the rows of data
		    while ( have_rows('primary_knap') ) : the_row();

		        if( get_row_layout() == 'eksternt_link' ):
		?>
		        <div class="nav">
		        	<a href="<?php the_sub_field('knap_url'); ?>" class="btn green" target="_blank"><?php the_sub_field('knap_tekst');?></a>
		        </div>
		<?php        	     	
		        endif;

		    endwhile;

		else :

		    // no layouts found

		endif;

		?>

	      
	      </div>