<?php

	$tlUrl = get_sub_field('tidslinje');
	$btnTxt = get_sub_field('knaptekst');
	$infoTxt  = get_sub_field('infotekst');
?>


<div class="tlContainer">
<!--iframe onmousewheel="" frameborder="0" scrolling="no" style="border-width:0;" id="tl-timeline-iframe" src="https://www.tiki-toki.com/timeline/embed/961200/4764318180/"></iframe-->
  
<iframe frameborder="0" scrolling="no" style="border-width:0;" id="tl-timeline-iframe" src="<?php echo $tlUrl; ?>"></iframe>
<!-- søg kildekode for embedHash for at finde urler til skoletube udgaverne -->

  <div>
    <a class="btn white"><?php echo $btnTxt; ?></a>
  </div>

  <p><em><?php echo $infoTxt; ?></em></p>
</div>





