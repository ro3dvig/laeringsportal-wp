		<?php

		$post_object = get_field('top_felt');

		if( $post_object ): 

			// override $post
			$post = $post_object;
			setup_postdata( $post ); 

				$bgToggle = get_field('bg_toggle');
				$topImage = get_field('baggrundsbillede');
				$topColor = get_field('baggrundsfarve');
				$logo = get_field('logo');
				$manchet = get_field('beskrivelse');


				if( $bgToggle != 'color' ) :
					$topBg = 'background-image: url(' . $topImage['url'] . ');';
				else: 
					$topBg = 'background-color: ' . $topColor . ';';
				endif;
		?>

    	<a id="main-content"></a>

		<div class="item">
    		
		    <div class="content">
		    	<div class="bg">
			        <h2 class="title"><?php the_title(); ?></h2>
			        <img class="logo" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>"/>
			        <p class="manchet"><?php echo $manchet;?></p>

			        <table class="info">
			        	<tr>
			        		<td>Niveau:</td>
			        		<td><?php the_terms( $post->ID, 'niveau', '', ', ' ); ?></td>
			        	</tr>
			        	<tr>
			        		<td>Fag:</td>
			        		<td><?php the_terms( $post->ID, 'fag', '', ', ' ); ?></td>
			        	</tr>

			        	<?php 			  
				        	if(has_term( '', 'emner')):
				        	?>

				        	<tr>
				        		<td>Emner:</td>
				        		<td><?php the_terms( $post->ID, 'emner', '', ', ' ); ?></td>
				        	</tr>

				        	<?php
				        	endif;
			        	?>
			        </table>

			        <div class="nav">
			        	<?php

						// check if the flexible content field has rows of data
						if( have_rows('primary_knap') ):

						     // loop through the rows of data
						    while ( have_rows('primary_knap') ) : the_row();

						        if( get_row_layout() == 'internt_link' ):
						?>
						        	
						        	<a href="<?php the_sub_field('knap_url'); ?>" class="btn green"><?php the_sub_field('knap_tekst');?></a>

						<?php        	
						        elseif( get_row_layout() == 'eksternt_link' ): 
						?>    
						        	<a href="<?php the_sub_field('knap_url'); ?>" class="btn green" target="_blank"><?php the_sub_field('knap_tekst'); ?></a>

						<?php        	
						        endif;

						    endwhile;

						else :

						    // no layouts found

						endif;

						?>				        	

					        	
			        	<?php
						// check if the flexible content field has rows of data
						if( have_rows('knapper') ):

						     // loop through the rows of data
						    while ( have_rows('knapper') ) : the_row();

						        if( get_row_layout() == 'internt_link' ):
						?>
						        	<a href="<?php the_sub_field('knap_url'); ?>" class="btn white"><?php the_sub_field('knap_tekst');?></a>

						<?php        	
						        elseif( get_row_layout() == 'eksternt_link' ): 
						?>    
						        	<a href="<?php the_sub_field('knap_url'); ?>" class="btn white" target="_blank"><?php the_sub_field('knap_tekst'); ?></a>

						<?php        	
						        endif;
						    endwhile;
						else :
						    // no layouts found
						endif;
						?>
			       	</div>
		       </div>

		    </div>


		    <!--Media field-->
		    <?php

			// check if the flexible content field has rows of data
			if( have_rows('tilfoj_medie') ):
			?>	
				<div class="media">
			<?php
			     // loop through the rows of data
			    while ( have_rows('tilfoj_medie') ) : the_row();

			        if( get_row_layout() == 'video' ):
			?>
					<div class="videoWrapper">
						<?php the_sub_field('youtube_id'); ?>
					</div>

			<?php	
			        elseif( get_row_layout() == 'billeder' ): 
			        	$img = get_sub_field('billede');
			?>
					<img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt'] ?>" />
			<?php
			        	
			        endif;

			    endwhile;
			?>
			    </div>

			<?php
			else :

			    // no layouts found

			endif;

			?>



		    <div class="overlay"></div>
		    <div class="background" style="<?php echo $topBg; ?>"></div>
			    
		</div>	


		<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
		<?php endif; ?>