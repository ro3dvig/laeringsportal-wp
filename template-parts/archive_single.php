<div class="archive_series archive_single additional_content_block">
	<h3>Se arkivalierne her</h3>

	<select class="sort">
		<option value="title">Sorter efter titel</option>
		<option value="year">Sorter efter årstal</option>
	</select>

	<ul>
	<?php
		if( have_rows('records') ): while ( have_rows('records') ) : the_row();
	?>
		<li data-year="<?php the_sub_field('year_start'); ?>" data-title="<?php the_sub_field('title'); ?>">
			<span class="fa fa-file-o icon"></span>
			<span class="year">Årstal: <?php the_sub_field('year_start'); ?>
				<?php if(get_sub_field('year_end')): echo ' - ' . get_sub_field('year_end'); endif; ?>
			</span>
			<a href="<?php the_sub_field('url'); ?>" target="_blank"><?php the_sub_field('title'); ?></a>
			
		</li>
	<?php endwhile; else: endif; ?>
	</ul>
</div>
