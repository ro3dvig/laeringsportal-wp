	<!-- section -->
		
			<div id="section1">
			    
			    	<h1><?php the_field('karusel_titel'); ?></h1>
			    	<div class="left"></div>
			         <div class="right"></div>	

			<?php 

			$posts = get_field('karusel');

			if( $posts ): ?>
			    
			   		<div class="grid-carousel">


			    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
			        <?php setup_postdata($post); ?>


			        	<?php
		        		$bgToggle = get_field('bg_toggle');
						$topImage = get_field('baggrundsbillede');
						$topColor = get_field('baggrundsfarve');

						$ribbon = get_field('ribbon');
						$ribbonColor =get_field('ribbon-color');
						$ribbonText =get_field('ribbon-text');
	


						if( $bgToggle != 'color' ) :
							$topBg = 'background-image: url(' . $topImage['url'] . ');';
						else: 							
							$topBg = 'background-color: ' . $topColor . ';';
						endif;

						$logo = get_field('logo');
						$manchet = get_field('beskrivelse');
						$content = get_field('indhold');

						$externalUrl = get_field('ekstern_url');
						?>
					
			        

			        <div class="carousel-grid-cell">
			          
			          <div class="item">

			          		<?php 

							$isRibb = $ribbon;

							if( $isRibb ): ?>
						    <div class="ribbon <?php echo $ribbonColor ?> ribbon-top-right"><span><?php echo $ribbonText; ?></span></div>
						    <?php endif; ?>

						    <div class="content">

						        
						        <img class="logo" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>"/>
						        
						        <?php

						        $currPT = get_post_type();


						        
						        if ( is_page() && $post->post_parent ) {
						        ?>
								    <h3 class="title"><?php the_title();  ?></h3>
								<?php } ?>


						        <p class="manchet"><?php echo $manchet; ?></p>
						        
						        <table class="info">
						        	<tr>
						        		<td>Niveau:</td>
						        		<td><?php the_terms( $post->ID, 'niveau', '', ', ' ); ?></td>
						        	</tr>
						        	<tr>
						        		<td>Fag:</td>
						        		<td><?php the_terms( $post->ID, 'fag', '', ', ' ); ?></td>
						        	</tr>
								<?php 			  
						        	if(has_term( '', 'emner')):
						        	?>

						        	<tr>
						        		<td>Emner:</td>
						        		<td><?php the_terms( $post->ID, 'emner', '', ', ' ); ?></td>
						        	</tr>

						        	<?php
						        	endif;
					        	?>
						        </table>

						        <div class="nav">  	
						        	<?php

									// check if the flexible content field has rows of data
									if( have_rows('primary_knap') ):

									     // loop through the rows of data
									    while ( have_rows('primary_knap') ) : the_row();

									        if( get_row_layout() == 'internt_link' ):
									?>
									        	
									        	<a href="<?php the_sub_field('knap_url'); ?>" class="btn green"><?php the_sub_field('knap_tekst');?></a>

									<?php        	
									        elseif( get_row_layout() == 'eksternt_link' ): 
									?>    
									        	<a href="<?php the_sub_field('knap_url'); ?>" class="btn green" target="_blank"><?php the_sub_field('knap_tekst'); ?></a>

									<?php        	
									        endif;

									    endwhile;

									else :

									    // no layouts found

									endif;

									?>	

						        	
						        	<?php

									// check if the flexible content field has rows of data
									if( have_rows('knapper') ):

									     // loop through the rows of data
									    while ( have_rows('knapper') ) : the_row();

									        if( get_row_layout() == 'internt_link' ):
									?>
									        	
									        	<a href="<?php the_sub_field('knap_url'); ?>" class="btn white"><?php the_sub_field('knap_tekst');?></a>

									<?php        	
									        elseif( get_row_layout() == 'eksternt_link' ): 
									?>    
									        	<a href="<?php the_sub_field('knap_url'); ?>" class="btn white" target="_blank"><?php the_sub_field('knap_tekst'); ?></a>

									<?php        	
									        endif;

									    endwhile;

									else :

									    // no layouts found

									endif;

									?>						        	
						       	</div>
						    </div>

						    <!--Media field-->
						    <?php

							// check if the flexible content field has rows of data
							if( have_rows('tilfoj_medie') ):
							?>	
								<div class="media">
							<?php
							     // loop through the rows of data
							    while ( have_rows('tilfoj_medie') ) : the_row();

							        if( get_row_layout() == 'video' ):
							?>
									<div class="videoWrapper">
										<?php the_sub_field('youtube_id'); ?>
									</div>

							<?php	
							        elseif( get_row_layout() == 'billeder' ): 
							        	$img = get_sub_field('billede');
							?>
									<img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt'] ?>" />
							<?php
							        	
							        endif;

							    endwhile;
							?>
							    </div>

							<?php
							else :

							    // no layouts found

							endif;

							?>

						    <div class="overlay"></div>

						    <div class="background" style="<?php echo $topBg; ?>"></div>
						    
						</div>	


			        	<div class="open"><span class="fa fa-chevron-down" aria-hidden="true" title="Åbn læringstilbud"></span></div>          
			        </div>


			    <?php endforeach; ?>

			    	</div> 
			    
			    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			<?php endif; ?>

			</div>
