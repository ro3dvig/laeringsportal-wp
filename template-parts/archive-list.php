	<?php
	$bgToggle = get_field('bg_toggle');
	$topImage = get_field('baggrundsbillede');
	$topColor = get_field('baggrundsfarve');

	if( $bgToggle != 'color' ) :
		$topBg = 'background-image: url(' . $topImage['sizes']['medium'] . ');';
	else: 
		$topBg = 'background-color: ' . $topColor . ';';
	endif;


	$logo = get_field('logo');
	$manchet = get_field('beskrivelse');
	$content = get_field('indhold');



	?>

	<article class="tag-el" aria-labelledby="article-<?php echo $count; ?>-title">


		<div class="thumb" style="<?php echo $topBg;?>">
			<div class="inner">
				<img class="logo" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>"/>
			</div>
		</div>

		<div class="teaser">
			<h2 id="article-<?php echo $count; ?>-title"><?php the_title(); ?></h2>

			<ul>

			<?php 			  
	        	if(has_term( '', 'niveau')):
	        	?>
			<li class="niveau">
				<p><strong>Niveau:</strong> <?php the_terms( $post->ID, 'niveau', '', ', ' ); ?></p>
			</li>
	        <?php
	        	endif;
        	?>

        	<?php 			  
	        	if(has_term( '', 'fag')):
	        	?>
			<li class="fag">
				<p><strong>Fag:</strong> <?php the_terms( $post->ID, 'fag', '', ', ' ); ?></p>
			</li>
	        <?php
	        	endif;
        	?>
	
			<?php 			  
	        	if(has_term( '', 'emner')):
	        	?>
			<li class="emner">
				<p><strong>Emner:</strong> <?php the_terms( $post->ID, 'emner', '', ', ' ); ?></p>
			</li>
	        <?php
	        	endif;
        	?>


        	</ul>

			<p><?php echo $manchet; ?></p>

			<div class="nav">  	
	        	<?php

				// check if the flexible content field has rows of data
				if( have_rows('primary_knap') ):

				     // loop through the rows of data
				    while ( have_rows('primary_knap') ) : the_row();

				        if( get_row_layout() == 'internt_link' ):
				?>
				        	
				        	<a href="<?php the_sub_field('knap_url'); ?>" class="btn green"><?php the_sub_field('knap_tekst');?></a>

				<?php        	
				        elseif( get_row_layout() == 'eksternt_link' ): 
				?>    
				        	<a href="<?php the_sub_field('knap_url'); ?>" class="btn green" target="_blank"><?php the_sub_field('knap_tekst'); ?></a>

				<?php        	
				        endif;

				    endwhile;

				else :

				    // no layouts found

				endif;

				?>		        	

	        	<?php

				// check if the flexible content field has rows of data
				if( have_rows('knapper') ):

				     // loop through the rows of data
				    while ( have_rows('knapper') ) : the_row();

				        if( get_row_layout() == 'internt_link' ):
				?>
				        	
				        	<a href="<?php the_sub_field('knap_url'); ?>" class="btn white"><?php the_sub_field('knap_tekst');?></a>

				<?php        	
				        elseif( get_row_layout() == 'eksternt_link' ): 
				?>    
				        	<a href="<?php the_sub_field('knap_url'); ?>" class="btn white" target="_blank"><?php the_sub_field('knap_tekst'); ?></a>

				<?php        	
				        endif;

				    endwhile;

				else :

				    // no layouts found

				endif;

				?>						        	
	       	</div>

	       	
		</div>

		


	<?php //edit_post_link(); ?>

	</article>
	<!-- /article -->