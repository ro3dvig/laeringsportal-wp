      <?php

		// check if the flexible content field has rows of data
		if( have_rows('indhold') ):

		     // loop through the rows of data
		    while ( have_rows('indhold') ) : the_row();

		        if( get_row_layout() == 'fritekst' ):

		        	the_sub_field('tekst');

		        elseif( get_row_layout() == 'img_column' ): 

		        	$columnImage = get_sub_field('billede');
		        	$columnAlt = $columnImage['alt'];

		       	?>
		        	<div class="img-column">
		        		<img src="<?php echo esc_url($columnImage['sizes']['thumbnail']);  ?>" alt="<?php echo esc_attr($columnAlt);  ?>"/>
		        		<span><?php the_sub_field('tekst'); ?></span>	
					</div>
				<?php

		        elseif( get_row_layout() == 'video' ): 

		        	echo '<div class="videoWrapper">';
						the_sub_field('youtube_url');
					echo '</div>';

				elseif( get_row_layout() == 'accordion' ): 
				?>
		        	
		        	<span class="accordion_toggleAll">
						<span>+ Åben alle</span><span>- Luk alle</span>
					</span>
					<ul class="accordion additional_content_block">
						

						<?php

						// check if the repeater field has rows of data
						if( have_rows('indholdsfelt') ):

						 	// loop through the rows of data
						    while ( have_rows('indholdsfelt') ) : the_row();
						    ?>

						    	<li>
									<input type="checkbox" checked>
									<h5><?php the_sub_field('title');?></h5>
									<span class="state"></span>
									<div class="accordion-content"><?php the_sub_field('content');?></div>
								</li>

							<?php

						    endwhile;
						else :

						    // no rows found

						endif;

						?>	

			
					</ul>

				<?php
				elseif( get_row_layout() == 'kildekatalog' ): 


					require_once( get_template_directory() . '/template-parts/kildekatalog.php' );


				elseif( get_row_layout() == 'kildepakker' ): 

					require_once( get_template_directory() . '/template-parts/archive_series.php' );


				elseif( get_row_layout() == 'tidslinjer' ): 

					include( get_template_directory() . '/template-parts/tidslinjer.php' );	
				

		        endif;

		    endwhile;

		else :

		    // no layouts found

		endif;

		?>


