<?php 

		$posts = get_field('sidebar');

		if( $posts ): ?>
		    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
		        <?php setup_postdata($post); ?>
		        <div class="box secondary">
		        	<h4><?php the_field('display_titel'); ?></h4>

		        	 <?php
				   				// check if the flexible content field has rows of data
		if( have_rows('indhold') ):

		     // loop through the rows of data
					    while ( have_rows('indhold') ) : the_row();

					        if( get_row_layout() == 'fritekst' ):

					        	the_sub_field('tekst');

					        elseif( get_row_layout() == 'video' ): 

					        	echo '<div class="videoWrapper">';
									the_sub_field('youtube_url');
								echo '</div>';

					        endif;

					    endwhile;

					else :

					    // no layouts found

					endif;
				    ?>
		        </div>
		    <?php endforeach; ?>
		    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
		<?php endif; ?>