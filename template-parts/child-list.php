<?php 
			$id = $post->ID;

			 $gp_args = array(
				'post_type' => 'tilbud',
				'post_parent' => $id,
				'orderby' =>  'menu_order', 
				'order' => 'ASC',
				'posts_per_page' => -1
			 );

			$posts = get_posts($gp_args);

			$count = 0;

			if( $posts ): ?>

	    <section>
	    	

	      <h2><?php the_field('relaterede_tilbud_titel'); ?></h2>

				<!--ul-->
				<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
					<?php setup_postdata($post); ?>

						<?php
						$bgToggle = get_field('bg_toggle');
						$topImage = get_field('baggrundsbillede');
						$topColor = get_field('baggrundsfarve');

						if( $bgToggle != 'color' ) :
							$topBg = 'background-image: url(' . $topImage['sizes']['medium'] . ');';
						else: 
							$topBg = 'background-color: ' . $topColor . ';';
						endif;

						?>

				   <article class="tag-el" aria-labelledby="article-<?php echo $count; ?>-title">

				   		<!--div class="thumb" style="background-image: url(<?php echo $thumb; ?>);">

				   			<img class="thumb" src="<?php echo $logo['url']; ?>" alt=""/>

				   		</div-->

				   		<div class="thumb" style="<?php echo $topBg;?>">
				   			<div class="inner">
								<img class="logo" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>"/>
							</div>
						</div>


				   		<div class="teaser">
							<h2 id="article-<?php echo $count; ?>-title"><?php the_title(); ?></h2>

							<div class="emner">
								<p>Emner: <?php the_terms( $post->ID, 'emner', '', ', ' ); ?></p>
							</div>

							<p><?php echo $manchet; ?></p>

							<div class="nav">  	
					        	<?php

								// check if the flexible content field has rows of data
								if( have_rows('primary_knap') ):

								     // loop through the rows of data
								    while ( have_rows('primary_knap') ) : the_row();

								        if( get_row_layout() == 'internt_link' ):
								?>
								        	
								        	<a href="<?php the_sub_field('knap_url'); ?>" class="btn green"><?php the_sub_field('knap_tekst');?></a>

								<?php        	
								        elseif( get_row_layout() == 'eksternt_link' ): 
								?>    
								        	<a href="<?php the_sub_field('knap_url'); ?>" class="btn green" target="_blank"><?php the_sub_field('knap_tekst'); ?></a>

								<?php        	
								        endif;

								    endwhile;

								else :

								    // no layouts found

								endif;

								?>	
					        	
					       	</div>

				    </article>

				    <?php $count++; ?>
				<?php endforeach; ?>
				<!--/ul-->
				<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			


	    </section>

			<?php endif; ?>




