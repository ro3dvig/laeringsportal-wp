<?php	
	$archive_posts = get_sub_field('archives');
	if( $archive_posts ):
?>

	<div class="archive_series additional_content_block">
	
	<h3>Se arkivserier her</h3>

	<select class="sort">
		<option value="title">Sorter efter titel</option>
		<option value="year">Sorter efter årstal</option>
	</select>

	<ul>
    <?php foreach( $archive_posts as $post): // variable must be called $post (IMPORTANT) ?>
   
   		<li data-year="<?php the_field('year_start'); ?>" data-title="<?php the_title(); ?>">
			<span class="fa fa-folder-open-o icon"></span>
			<span class="year">Årstal: <?php the_field('year_start'); ?>
				<?php if(get_field('year_end')): echo ' - ' . get_field('year_end'); endif; ?>
			 </span>
			 <a href="<?php the_permalink(); ?>" target="_blank"><?php the_title(); ?></a>
		</li>

	<?php 
		endforeach;
		wp_reset_postdata();
	?>
	</ul>
	
<?php
	endif; 
?>
