<?php get_header(); ?>



    <a id="main-content"></a>
		
	<main id="page" class="tag-list">
		
		<h1><?php the_archive_title(); ?></h1>

		<!-- section -->
		<section class="full">


		<?php
			global $post; // required

			 
			if ( get_query_var('paged') ) {
				$paged = get_query_var('paged');
			} elseif ( get_query_var('page') ) {
				$paged = get_query_var('page');
			} else {
			   $paged = 1;
			}
			 
			$args = array(
			  'posts_per_page' => 10,
			  'post_type' => 'tilbud',
			  'post_parent' => 0,
			  'paged' => $paged
			);
			 
			$custom_query = new WP_Query( $args );
			 
			$temp_query = $wp_query;
			$wp_query = null;
			$wp_query = $custom_query;
			 
			if ( $custom_query->have_posts() ):
			  while( $custom_query->have_posts() ):
			     $custom_query->the_post();
			     ?> 

			     <?php include( get_template_directory() . '/template-parts/archive-list.php' ); ?>	

			     <?php
			  endwhile;
			endif;
			 
			wp_reset_postdata();
			 	 
			// reset the main query object to avoid 
			// unexpected results on other parts of the page   
			$wp_query = NULL;
			$wp_query = $temp_query;
		?>

			<div class="pagination">
			<?php
			global $wp_query;
			    $big = 999999999;
			    echo paginate_links(array(
			        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
			        'format' => '?paged=%#%',
			        'current' => max(1, get_query_var('paged')),
			        'total' => $custom_query->max_num_pages
			    ));

			?>
			</div>

		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
