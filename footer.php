		
		</main>

		
		<footer class="footer">
			<div class="logo">
				<img src="<?php echo get_template_directory_uri(); ?>/img/ra-logo-vandret-w.svg" alt="Logo Rigsarkivet">
			</div>



			<section>
				<div class="col">
					<h3>Links</h3>

					<?php if( have_rows('internal_links', 'option') ): ?>

					    <ul>

					    <?php while( have_rows('internal_links', 'option') ): the_row(); ?>

					        <li><a href="<?php the_sub_field('link_page'); ?>"><?php the_sub_field('link_text'); ?></a></li>

					    <?php endwhile; ?>

					    </ul>

					<?php endif; ?>

				</div>

				<div class="col">
					<h3>Eksterne Links</h3>
					<?php if( have_rows('eksternal_links', 'option') ): ?>

					    <ul>

					    <?php while( have_rows('eksternal_links', 'option') ): the_row(); ?>

					        <li><a href="<?php the_sub_field('link_url'); ?>" target="_blank"><?php the_sub_field('link_text'); ?></a></li>

					    <?php endwhile; ?>

					    </ul>

					<?php endif; ?>	
				</div>

				<div class="col">
					<h3>Kontakt</h3>
					<div class="contact">
						<?php the_field('contact', 'option'); ?>		
					</div>
				</div>

			</section>



				

		</footer>

	<!--/div-->  

		<?php wp_footer(); ?>

		<!-- modal box -->
	    <div id="modal">    
	        <div class="close"><span class="fa fa-times"></span></div>        
	    </div>
  
	</body>
</html>
