<?php get_header(); ?>

    <a id="main-content"></a>
		
	<main id="page" class="tag-list">
		
		<h1><?php the_archive_title(); ?></h1>

		<!-- section -->
		<section class="full">

			<?php 
				
				$count = 0;

				if (have_posts()): while (have_posts()) : the_post(); 

			?>


			<?php include( get_template_directory() . '/template-parts/archive-list.php' ); ?>	

				<?php $count++; ?>

			<?php endwhile; ?>

			<?php else: ?>

				<!-- article -->
				<article>
					<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
				</article>
				<!-- /article -->

			<?php endif; ?>




			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
