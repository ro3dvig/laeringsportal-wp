<?php 

	
	$topImage = get_field('header_billede');
	$manchet = get_field('manchet');
	//$content = get_field('indhold');

	$isSidebar = get_field('sidebar');

	//sidebar
?>



<?php get_header(); ?>

	  <header>
	    <div id="page-header" style="background-image: url('<?php echo $topImage['url']; ?>');"></div>
	  </header>

	<a id="main-content"></a>

	<main id="page">
		<nav aria-label="Brødkrummesti" class="breadcrumb">
			<?php if(function_exists('bcn_display')) { bcn_display(); }?>
		</nav>
		
  		<h1><?php the_title(); ?></h1> 


	  	<?php 
	  	if( $isSidebar ) :
	   		echo '<section>';		  
		else: 
			echo '<section class="full">';		  
	   	endif;
	    ?>
      

	    <?php
      	if($manchet):
      		echo '<p class="manchet">' . $manchet . '</p>';
      	endif;	      
	    ?>

	    <?php
	   		require_once( get_template_directory() . '/template-parts/flex-content.php' );
	    ?>

	    </section>





	    <?php 
	    if( $isSidebar ) :
	    echo '<aside>';		  
		    require_once( get_template_directory() . '/template-parts/widget.php' );
	   	echo  '</aside>';
	   	endif;
	    ?>	


	</main>


<?php get_footer(); ?>
