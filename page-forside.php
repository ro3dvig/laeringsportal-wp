<?php /* Template Name: Forside */ ?>

<?php get_header(); ?>


	<header id="promotion">
	     <?php require_once( get_template_directory() . '/template-parts/promotion.php' ); ?>
	</header>



	<main id="frontpage">

		<section>		
	    	<?php require_once( get_template_directory() . '/template-parts/carousel.php' ); ?>	
		</section>	
		



		<?php 

		$posts = get_field('page_reference');

		if( $posts ): ?>
		    <section>
		    	<h2><?php the_field('referencer_titel'); ?></h2>
		    	<div class="boxgrid">
		    		<ul class="clearfix">


				    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
				        <?php setup_postdata($post); 

				        	$headerImg = get_field('header_billede');
				        	$refManchet = get_field('manchet');
				        ?>
				        <li>
				        	<figure>
								<div class="boxhead" style="background-image: url('<?php echo $headerImg['sizes']['medium']; ?>');" alt=""></div>							
								<figcaption>
									<h3><?php the_title(); ?></h3>
									<p><?php echo $refManchet; ?></p>

									<?php

									// check if the flexible content field has rows of data
									if( have_rows('knapper') ):

									     // loop through the rows of data
									    while ( have_rows('knapper') ) : the_row();

									        if( get_row_layout() == 'internt_link' ):
									?>
									        	
									        	<a href="<?php the_sub_field('knap_url'); ?>" class="btn white"><?php the_sub_field('knap_tekst');?></a>

									<?php        	
									        elseif( get_row_layout() == 'eksternt_link' ): 
									?>    
									        	<a href="<?php the_sub_field('knap_url'); ?>" class="btn green" target="_blank"><?php the_sub_field('knap_tekst'); ?></a>

									<?php        	
									        endif;

									    endwhile;

									else :

									    // no layouts found

									endif;

									?>	

								</figcaption>
							</figure>
				        </li>
				    <?php endforeach; ?>


		    		</ul>
		    	</div>
		    </section>
		    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
		<?php endif; ?>


	</main>


<?php get_footer(); ?>
